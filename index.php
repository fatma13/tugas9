<?php
require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");
echo "<br>";
echo "name : {$sheep->name}\n<br>";
echo "legs : {$sheep->legs}\n<br>";
echo "cold blooded: {$sheep->cold_blooded}\n\n<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "<br>";
echo "name : {$kodok->name}\n<br>";
echo "legs : {$kodok->legs}\n<br>";
echo "cold blooded: {$kodok->cold_blooded}\n<br>";
$kodok->jump();
echo "\n";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "<br>";
echo "name : {$sungokong->name}\n<br>";
echo "legs : {$sungokong->legs}\n<br>";
echo "cold blooded: {$sungokong->cold_blooded}\n<br>";
$sungokong->yell();