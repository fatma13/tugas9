<?php
class Ape extends Animal {
    public function __construct($name) {
        parent::__construct($name);
        $this->legs = 2; // Mengatur jumlah kaki menjadi 2 untuk kelas Ape
    }

    public function yell() {
        echo "yell: Auooo\n";
    }
}
?>
