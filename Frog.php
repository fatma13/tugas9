<?php
class Frog extends Animal {
    public function __construct($name) {
        parent::__construct($name);
        $this->legs = 4; // Mengatur jumlah kaki menjadi 2 untuk kelas Ape
    }

    public function jump() {
        echo "jump: Hop hop\n";
    }
}

?>
